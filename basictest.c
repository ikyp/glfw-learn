#include <GLFW/glfw3.h>
#include <stdio.h>

static void error_callback(int error, const char * des)
{
	fprintf(stderr, "Error: %s\n", des);
}

int main(void)
{
	GLFWwindow *window;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit()) {
		return -1;
	}

	window = glfwCreateWindow(640, 400, "Hello GLFW", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}